import { ScrollView, StyleSheet, Text, View } from 'react-native';
import Card from '../components/Card';
import { getRecipe } from '../utils/data';
import { useEffect, useState } from 'react';
import { SafeAreaView } from 'react-native-safe-area-context';

export default function Home() {

    const [data, setData] = useState([])

    useEffect(async () => {
        Promise.all([
            getRecipe(), getRecipe(), getRecipe()
        ]).then((values) => {
            setData(values)
        })

    }, [])

    return (
        <SafeAreaView style={styles.container}>
            <ScrollView style={styles.recipesContainer}>
                {data.map((recipe) => (
                    recipe.map((item, index) => (
                        <Card recipe={item} key={index} />
                    ))
                ))}
            </ScrollView>
        </SafeAreaView>
    );
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: "#fcf7f1",
    },
    recipesContainer: {
    },
});
import { StyleSheet, Text, View, Image, ScrollView } from 'react-native'
import React from 'react'
import Title from '../components/Title';
import { useState, useEffect } from 'react';
import IngredientCard from '../components/IngredientCard';
import Subtitle from '../components/Subtitle';

const Recipe = ({ route, navigation }) => {

    const { recipe } = route.params;

    const [ingredients, setIngredients] = useState([])




    useEffect(() => {
        const newIngredients = [];

        for (let i = 1; i <= 20; i++) {
            const ingredient = recipe['strIngredient' + i];
            if (ingredient && ingredient !== '') {
                newIngredients.push(ingredient);
            }
        }

        setIngredients(newIngredients);
    }, [recipe]);



    return (
        <ScrollView>
            <Image source={{
                uri: recipe.strMealThumb,
            }} style={styles.imageBanner} />
            <Title content={recipe.strMeal} />
            <View>
                <Subtitle content="Ingedients :"/>
                <ScrollView horizontal={true}>
                    {ingredients.map((ingredient, index) => (
                        <IngredientCard ingredientName={ingredient} key={index}/>
                    ))}
                </ScrollView>
            </View>
            <View>
                <Subtitle content="Instructions :"/>
                <Text>{recipe.strInstructions}</Text>
            </View>
        </ScrollView>
    )
}

export default Recipe

const styles = StyleSheet.create({
    imageBanner: {
        width: '100%',
        height: 200,
        resizeMode: 'stretch',
    },
})
import Home from './screen/Home';
import * as React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import Recipe from './screen/Recipe';

export default function App() {

  const Stack = createNativeStackNavigator();

  return (
    <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen name="Home" component={Home} options={{ title: 'Random Recipes' }}/>
        <Stack.Screen name="Recipe" component={Recipe}/>
      </Stack.Navigator>
    </NavigationContainer>
  );
}

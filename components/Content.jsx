import { StyleSheet, Text, View } from 'react-native'
import React from 'react'

const Content = ({content}) => {
  return (
      <Text style={styles.contentCard}>{content}</Text>
  )
}


const styles = StyleSheet.create({
    contentCard: {
        fontSize: 18,
        textAlign: 'justify',
        padding: 5,
    },
})

export default Content

import { StyleSheet, Text } from 'react-native'
import React from 'react'

const Title = ({content}) => {
  return (
      <Text style={styles.title}>{content}</Text>
  )
}


const styles = StyleSheet.create({
    title: {
        fontSize: 25,
        fontWeight: 'bold',
        textAlign: 'center',
        marginBottom: 10,
        paddingTop: 10,
        paddingBottom: 10,
    },
})

export default Title

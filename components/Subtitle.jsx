import { StyleSheet, Text, View } from 'react-native'
import React from 'react'

const Subtitle = ({content}) => {
  return (
    <Text style={styles.Subtitle}>{content}</Text>
  )
}

export default Subtitle

const styles = StyleSheet.create({
    Subtitle: {
        fontSize: 18,
        fontWeight: 'bold',
        textAlign: 'left',
        padding: 5,
    },
})
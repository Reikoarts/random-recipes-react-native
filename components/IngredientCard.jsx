import { StyleSheet, Text, View } from 'react-native'
import React from 'react'

const IngredientCard = ({ingredientName}) => {
  return (
    <View style={styles.IngredientCard}>
        <Text>{ingredientName}</Text>
    </View>
  )
}

export default IngredientCard

const styles = StyleSheet.create({
    IngredientCard: {
        backgroundColor: '#fff',
        borderRadius: 5,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 1,
        },
        shadowOpacity: 0.20,
        shadowRadius: 1.41,
        elevation: 2,
        margin: 5,
        width: 100,
        height: 100,
        justifyContent: 'center',
        alignItems: 'center',
    },

    imageCard: {
        width: '100%',
        height: 200,
        borderTopLeftRadius: 5,
        borderTopRightRadius: 5,
        resizeMode: 'stretch',
    },
})
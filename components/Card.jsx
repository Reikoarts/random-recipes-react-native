import React from 'react';
import { StyleSheet, View, Image, TouchableOpacity } from 'react-native';
import Title from './Title';
import Content from './Content';
import { useNavigation } from '@react-navigation/native';

const Card = ({ recipe }) => {
    const navigation = useNavigation();

    return (
        <TouchableOpacity
            onPress={() => {
                navigation.navigate('Recipe', {
                  recipe: recipe,
                });
              }}
        >
            <View style={styles.card}>
                <Image source={{
                    uri: recipe.strMealThumb,
                }} style={styles.imageCard} />

                <Title content={recipe.strMeal} />
            </View>
        </TouchableOpacity>
    );
}

const styles = StyleSheet.create({
    card: {
        backgroundColor: '#fff',
        borderRadius: 5,
        width: '90%',
        marginLeft: 'auto',
        marginRight: 'auto',
        marginTop: 10,
        marginBottom: 10,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 1,
        },
        shadowOpacity: 0.20,
        shadowRadius: 1.41,
        elevation: 2,
    },

    imageCard: {
        width: '100%',
        height: 200,
        borderTopLeftRadius: 5,
        borderTopRightRadius: 5,
        resizeMode: 'stretch',
    },
})

export default Card;
